/*!
 * jQuery scrollInTurn plugin -version 1.0.1
 *
 *
 * Copyright 2014, Mayumi Tanji
 * Released under the MIT license
 * http://www.opensource.org/licenses/mit-license.php
 */

 (function( $ ){

  $.fn.scrollInTurn = function( options ) {

    var windowHeight = $(window).height();
    var triggerPoint = windowHeight * 0.6;

    return this.each(function() {

      var self = $(this);

      $(window).on('load scroll resize',function(){
        var elmTop = self.offset().top;
        var scrTop = $(window).scrollTop();


        if(scrTop > elmTop - triggerPoint){
          self.find(".effect_target").each(function(i) {
            $(this).delay(i*350).queue(function() {
              $(this).addClass("animation");
            });
          });
        }
      });

    });

  };

})( jQuery );
