
$(function(){
	$(".effect").scrollInTurn();
});

$(function(){

	var touchEv = (window.ontouchstart === undefined) ? 'click' : 'touchstart click';
	var $win = $(window);
	var $html = $('html, body');
	var $body = $('body');

/* page scroll */
	$('a[href^="#"].sc').on(touchEv, function(e){
		e.preventDefault();

		if( $(this).is('.gotop') ){
			var speed = 600;
			$html.animate({scrollTop:0}, speed, 'swing');
		}else{
			var pad = 60;
			if( $(this).is('.pg') ){
				var pad = 95;
			}
			var url = this.href;
			var parts = url.split('#');
			var target = parts[1];
			var target_offset = $('#' + target).offset();
			var target_top = target_offset.top - pad;
			$html.animate({scrollTop:target_top}, 700);
		}
	});


/* iScroll Init */
	var naviSc;
	naviSc = new IScroll('#navi', {
		mouseWheel:true,
		preventDefault:false
	});



/* SP menuBtn */
	var $menuBtn = $('#menu');
	var $navi = $('#navi');
	$menuBtn.on(touchEv, function(){
		$html.addClass('open');
		$navi.addClass('open');

		if( $body.hasClass('open') ){
			$win.on('touchmove.noScroll', function(e){
				e.preventDefault();
			});
			naviSc.refresh();
		}
		return false;
	});

	$('a.btnClose', $navi).on(touchEv, function(){
		$html.removeClass('open');
		$navi.removeClass('open');

		if( $body.not('open') ){
			$win.off('.noScroll');
		}
		return false;
	});

	$(document).on(touchEv, function(e){
		if( $body.hasClass('open') && !$(e.target).closest('#navi').length ){
			$html.removeClass('open');
			$navi.removeClass('open');

			if( $body.not('open') ){
				$win.off('.noScroll');
			}
			return false;
		}
	});




});
